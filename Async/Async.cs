﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

public class AsyncTask
{
    public static void Main()
    {
        CallMethod();
    }

    public static async void CallMethod()
    {
        Task<int[]> taskOne = GenerateArray();

        Console.WriteLine(string.Join(",", taskOne.Result));

        Task<int[]> taskTwo = MultipleArray(await taskOne);

        Console.WriteLine(string.Join(",", taskTwo.Result));

        Task<int[]> taskThree = SortedArray(await taskTwo);

        Console.WriteLine(string.Join(",", taskThree.Result));

        Task<double> taskFour = Average(await taskThree);

        Console.WriteLine(string.Join(",", taskFour.Result));
    }

    public static async Task<int[]> GenerateArray()
    {
        int Min = 0;
        int Max = 20;

        int[] arr = new int[5];

        Random randNum = new();
        await Task.Run(() =>
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = randNum.Next(Min, Max);
            }
        });

        return arr;
    }

    public static async Task<int[]> MultipleArray(int[] arr)
    {
        int Min = 0;
        int Max = 5;
        Random random = new();
        int multiplier = random.Next(Min, Max);

        await Task.Run(() =>
        {
            for (int i = 0; i < arr.Length; ++i)
                arr[i] *= multiplier;
        });

        return arr;
    }

    public static Task<int[]> SortedArray(int[] arr)
    {
        return Task.FromResult(arr.OrderBy(x => x).ToArray());
    }

    public static Task<double> Average(int[] arr)
    {
        return Task.FromResult(Queryable.Average(arr.AsQueryable()));
    }
}